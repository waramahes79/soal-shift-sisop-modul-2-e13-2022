# soal-shift-sisop-modul-2-E13-2022



## Anggota Kelompok
Nama | NRP
---------- | ----------
Maheswara Danendra Satriananda | 5025201060
Luthfiyyah hanifah amari | 5025201090
Mohammad Fany Faizul Akbar | 5025201225

# Soal 1

untuk soal 1a, secara umum yang dilakukan ialah mendownload file dari link, ekstrak file, dan membuat folder gacha_gacha. untuk melakukannya, saya memanggil beberapa fungsi seperti pada kode berikut. fungsi-fungsi ini melakukan prosesnya menggunakan fork(), execv(), dan wait().
```
    char link_weapons[] = "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char link_characters[] = "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
    char file_weapons[] = "uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char file_characters[] = "uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";

    download(link_weapons);
    ekstrak(file_weapons);
    download(link_characters);
    ekstrak(file_characters);

    buatFolder("gacha_gacha");

```

- fungsi download memiliki parameter bertipe data char* (untuk meunjuk string) untuk menampung link (sumber download). untuk mendownload, digunakan command "wget" yang berada pada path "/usr/bin/wget". "-nd" artinya hasil download tidak memiliki directory (hasil download langsung berupa zip). 
```
    void download(char* link)
    {
        child_id = fork();

        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }
        if (child_id == 0) 
        {
            printf("child PID = %d\n", getpid());
            char *argv[] = {"/usr/bin/wget", "-nd", link, NULL};
            execv("/usr/bin/wget", argv);
        }           
        while ((wait(&status)) > 0); //program akan menunggu proses child selesai melakukan prosesnya.
    }
```

- fungsi ekstrak memiliki parameter bertipe data char* (untuk menunjuk string) untuk menampung nama file yang ingin di-unzip. untuk mendownload, digunakan command "unzip" yang berada pada path "/bin/unzip".

```
    void ekstrak(char* file)
    {
        child_id = fork();

        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id == 0) 
        {
            char *argv[] = {"/bin/unzip", file, NULL};
            execv("/bin/unzip", argv);
        } 
        while ((wait(&status)) > 0);
    }
```
- fungsi buatFolder memiliki parameter bertipe data char* (untuk menunjuk string) untuk menampung nama folder yang ingin dibuat. untuk mendownload, digunakan command "mkdir" yang berada pada path "/usr/bin/mkdir".
```
    void buatFolder(char* nama)
    {
        child_id = fork();

        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id == 0) 
        {
            char *argv[] = {"/usr/bin/mkdir", nama, NULL};
            execv("/usr/bin/mkdir", argv);
        } 
        while ((wait(&status)) > 0);
    }
```


untuk soal 1 b, c, d. idenya,
saya memasukkan semua nama file weapons dan characters ke dalam array multidimensi terlebih dahulu (untuk digunakan pada tahap selanjutnya). 

```
    char arr_weapons[1000][100];
    char arr_char[1000][100];

    // mengambil gacha item weapons jika genap
    int idw = 0;
    strcpy(path, "./weapons");

    dp = opendir(path);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            strcpy(arr_weapons[idw++], ep->d_name);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    // mengambil gacha item characters jika genap
    int idc = 0;
    strcpy(path, "./characters");

    dp = opendir(path);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            strcpy(arr_char[idc++], ep->d_name);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

```

Kemudian, program melakukan gacha. di sini, setiap gacha menghabiskan 160 primogems. program melakukan looping (gacha) hingga primogems tidak cukup lagi utk nge-gacha. 

```
while(primogems >= 160)
    {   
        // setiap gacha menghabiskan 160 primogems
        primogems -= 160;
        .
        . //isi code nya dijelaskan selanjutnya
        .
    }
```

code ini untuk menyimpan (string) path dari file yang diambil random. program memakail "characters/" dan "weapons/" karena file ada di folder tersebut. lalu, di concate dengan nama file yg random. nama file diambil dari array of string yang sebelumnya.
```
        int random = rand() % 47;
        char path_char[50] = "characters/";
        char path_weapons[50] = "weapons/";
        strcat(path_char, arr_char[random]);
        strcat(path_weapons, arr_weapons[random]);
```

selanjutnya, soal meminta untuk menulis pada file sesuai dengan format tulisan yang telah ditentukan. maka dari itu, idenya, sebelum teks dimasukkan ke file, program ini menyimpan teks (sesuai format tulisan) ke dalam array temp_gacha terlebih dulu. karena file berformat json, maka diperlukan fungsi-fungsi json. apabila genap, program mengambil gacha dari characters. apabila ganjil, program mengambil gacha dari weapons. di sini memakai fopen, fread, fclose karena program mengambil data dari file yang path-nya berada di variabel path_char atau path_weapons. di sini banyak strcat dan strcpy karena dibutuhkan untuk menulis format tulisan.
```
        sprintf(str_i, "%d", i);

        int i_temp = i % 10; //index untuk array yg menampung data sementara
        strcpy(temp_gacha[i_temp], str_i);

        if(i%2 == 0)
        {
            // jika genap, gacha diambil dari characters
            fp = fopen(path_char, "r");
            fread(buffer, 3000, 1, fp);
            fclose(fp);
            
            parsed_json = json_tokener_parse(buffer);
            json_object_object_get_ex(parsed_json, "name", &name);
            json_object_object_get_ex(parsed_json, "rarity", &rarity);

            strcpy(temp_gacha[i_temp], "_characters_");
        }
        else
        {
            // jika ganjil, gacha diambil dari weapons
            fp = fopen(path_weapons, "r");
            fread(buffer, 3000, 1, fp);
            fclose(fp);
            
            parsed_json = json_tokener_parse(buffer);
            json_object_object_get_ex(parsed_json, "name", &name);
            json_object_object_get_ex(parsed_json, "rarity", &rarity);
            strcpy(temp_gacha[i_temp], "_weapons_");
        }

        strcat(temp_gacha[i_temp], "");
        strcat(temp_gacha[i_temp], json_object_get_string(rarity));
        strcat(temp_gacha[i_temp], "_");
        strcat(temp_gacha[i_temp], json_object_get_string(name));
        strcat(temp_gacha[i_temp], "_");

        sprintf(str_primogems, "%d", primogems);
        strcat(temp_gacha[i_temp], str_primogems);

```

idenya, apabila jumlah gacha modulo 10, maka program membuat file. lalu, program menuliskan teks yang telah disimpan di array temp_gacha ke dalam file yg baru dibuat
```
        if(i%10 == 0)
        {

            time_t t = time(NULL);
            struct tm tm = *localtime(&t);
            sprintf(nama_file, "./gacha_gacha/%02d:%02d%02d_gacha_%d.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, i);

            strcpy(arr_file[i_file], nama_file);

            FILE *fp1;
            fp1 = fopen(nama_file, "w+");
            for(j = 1; j<10; j++)
            {
                fputs(temp_gacha[j], fp1);
                fputs("\n", fp1);
            }
            fputs(temp_gacha[0], fp1);
            fputs("\n", fp1);
            fclose(fp1);
        }

```

apabila jumlah gacha modulo 90, maka program diminta untuk membuat folder. maka dari itu, idenya, program membuat folder seperti berikut. program memanggil buatFolder() dengan parameter nama yg digunakan pada folder. selanjutnya, folder ingin diisi oleh file. dari code sebelumnya, kita telah berencana untuk membuat file. lalu, di sini kita ingin memindahkan file-file tsb ke dalam folder ini. di sini program menggunakan fork(), execv(), dan wait().  untuk memindahkan file, program menggunakan comman mv yg berada di path /usr/bin/mv
```
if(i%90 == 0)
        {
            // membuat folder baru dengan format nama total_gacha_{jumlah-gacha}
            sprintf(sjumlah, "%d", i);
            strcpy(nama_folder, "total_gacha_");
            strcat(nama_folder, sjumlah);

            buatFolder(nama_folder);
            
            for(j=0; j<10; j++)
            {
                child_id = fork();

                if (child_id < 0) {
                    exit(EXIT_FAILURE);
                }
                if (child_id == 0) 
                {
                    char *argv[] = {"/usr/bin/mv", arr_file[j], nama_folder, NULL};
                    execv("/usr/bin/mv", argv);
                } 
                while ((wait(&status)) > 0);
            }
            
        }
```


# Soal 2
Soal ini meminta kita untuk meng -extract folder yang ada dalam `drakor.zip` lalu memindahkan konten folder ke dalam `/home/[user]/shift2/drakor`. Setelah di extract langkah selanjutnya adalah untuk mengatur poster film sesuai genrenya serta menambahkan info dari poster film yang ada dalam bentuk file `.txt`

Berikut ini adalah function untuk pembuatan directory, dan unzip
```
void Createdir(){
    char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
    execv("/bin/mkdir", argv);
}

void unzip_drakor(){
    char *argv[] = {"unzip", "-n", "drakor.zip", "*.png", NULL};
    execv("/bin/unzip", argv);
}

void mkdir_custom(char* dir){
    char *argv[] = {"mkdir", "-p", dir, NULL};
    execv("/bin/mkdir", argv);
}
```

Pembuatan Folder kategori film dan juga pemisah nama dari sebuah file `.png`
```
void split(char* filename){
    char* token1;
    int count = 0;

    token1 = strtok(filename, "_;.");
   
    while( token1 != NULL ) {
        count++;
        if(count == 3 || count == 6){
            pid_t child_id;
            int status;
            child_id = fork();
            if (child_id < 0) exit(EXIT_FAILURE);

            if (child_id == 0) {
                mkdir_custom(token1);
            } else while ((wait(&status)) > 0);
        }
        
        token1 = strtok(NULL, "_;.");
    }
}

void dircheck(){
    DIR *dp;
    struct dirent *ep;

    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) split(ep->d_name);

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}
```

Pemindahan file ke masing masing folder genre serta merubah nama dari file yang ada setelah itu membuat `data.txt`
```
void png_move(){
    DIR *dp;
    struct dirent *ep;
    FILE *fptr1, *fptr2;

    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) {
        char* token1, pngname[3][50], dirname[300], temp, strtemp[280];
        int count = 0;
        FILE *fptr;
        strcpy(strtemp, ep->d_name);

        token1 = strtok(strtemp, "_;.");
    
        while( token1 != NULL ) {
            count++;
            if ((count == 1 || count == 4) && strcmp(token1, "png") != 0){
                strcpy(pngname[0], token1);
            }
            else if (count == 2 || count == 5){
                strcpy(pngname[1], token1);
            }
            else if (count == 3 || count == 6){
                strcpy(pngname[2], token1);
                sprintf(dirname, "%s/%s_%s_%s.png", pngname[2], pngname[1], pngname[0], pngname[2]);
                fptr1 = fopen(ep->d_name, "rb");
                fptr2 = fopen(dirname, "wb");

                while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

                fclose(fptr1);
                fclose(fptr2);
            }
            
            token1 = strtok(NULL, "_;.");
            }
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void genre_check(char *dir){
    DIR *dp;
    struct dirent *ep;
    int index = 0, count, status;
    char poster_name[10][280], temp[280], *token, name[280], year[280], dirname[100], temp2[290], temp3[290];
    FILE *fptr;
    pid_t child_id;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL){
          strcpy(poster_name[index++], ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    for(int i=0; i<index-1; i++)for (int j=0; j<index-i-1; j++){
        if(strcmp(poster_name[j], poster_name[j+1]) > 0){
            strcpy(temp, poster_name[j]);
            strcpy(poster_name[j], poster_name[j+1]);
            strcpy(poster_name[j+1], temp);
        }
    }
    sprintf(dirname, "%s/data.txt", dir);
    fptr = fopen(dirname, "w");
    fprintf(fptr, "kategori : %s\n", dir);
    for(int i=0; i<index; i++) {
        strcpy(temp, poster_name[i]);
        count = 0;
        token = strtok(temp, "_");
        while(token != NULL){
            count++;
            if(count == 1) strcpy(year, token);
            else if (count == 2) strcpy(name, token);
            token = strtok(NULL, "_");
        }
        fprintf(fptr, "\nnama : %s", name);
        fprintf(fptr, "\nrilis : %s\n", year);
        sprintf(temp3, "%s/%s.png", dir, name);
        sprintf(temp2, "%s/%s", dir, poster_name[i]);
        child_id = fork();
        if (child_id < 0) exit(EXIT_FAILURE);

        if (child_id == 0) {
                char *argv[] = {"mv", temp2, temp3, NULL};
                execv("/bin/mv", argv);

        } else while ((wait(&status)) > 0);
    }
    fclose(fptr);
}
```

Hasil run dari program tersebut

![image.png](./image.png)

![image-1.png](./image-1.png)

# Soal 3

Hal pertama yang harus dilakukan adalah membuat direktori dengan nama `darat`, lalu setelah 3 detik membuat direktori bernama `air`. Kedua folder tersebut berada pada direktori `/home/[USER]/modul2/`. Untuk membuat kedua direktori tersebut dengan jeda 3 detik, dapat dilakukan dengan menggunakan potongan kode berikut.
```
    child_id = fork();

    if (child_id < 0) exit(EXIT_FAILURE); 
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "home/fany/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    while ((wait(&status)) > 0);
    sleep(3);
    child_id = fork(); 
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", "home/fany/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    while((wait(&status)) > 0);
```

Setelah itu ekstrak `animal.zip` ke direktori `/home/[USER]/modul2/`. Untuk mengekstraknya, bisa dilakukan menggunakan potongan kode berikut.
```
    child_id = fork(); 
    if (child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0){
        char *argv[] = {"unzip","-d","home/fany/modul2", "animal", NULL};
        execv("/bin/unzip", argv);
    }  
    while((wait(&status)) > 0);
```

Kemudian hasil ekstrak dipisah ke folder `darat` dan `air` sesuai dengan keterangan yang ada di nama file. Untuk hewan yang tidak memiliki keterangan `darat` maupun `air`, maka akan dihapus. proses ini dapat diselesaikan menggunakan library `dirent.h` untuk mengakses nama setiap file seperti pada potongan kode berikut.
```
    DIR *dp;
    struct dirent *ep;
    char kewan[100] = "home/fany/modul2/animal";
    char banyu[100] = "home/fany/modul2/air";
    char darat[100] = "home/fany/modul2/darat";

    dp = opendir(kewan);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
            if(strstr(ep->d_name, "darat") != NULL){
                char src[200] = "home/fany/modul2/animal/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"mv",src,darat, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
            }
            if(strstr(ep->d_name, "air") != NULL){
                char src[200] = "home/fany/modul2/animal/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"mv",src,banyu, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
            }  
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE); 
    if(child_id == 0){
        char *argv[] = {"rm","-r","home/fany/modul2/animal/", NULL};
        execv("/bin/rm", argv);
    }
    while((wait(&status)) > 0);
```

Lalu, di direktori `darat`, dihapus semua file yang memiliki keterangan `bird`. proses ini dapat diselesaikan menggunakan potongan kode berikut.
```
    dp = opendir(darat);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
            if(strstr(ep->d_name, "bird") != NULL){
                char src[100] = "home/fany/modul2/darat/";
                strcat(src,ep->d_name);
                child_id = fork();
                if (child_id < 0) exit(EXIT_FAILURE); 
                if(child_id == 0){
                    char *argv[] = {"rm",src, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
            }
      }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
```

Terakhir, di direktori `air`, dibuat file `list.txt` yang menampung seluruh data file di direktori tersebut dengan format `UID_UID File Permission_File name.[jpg/png]`. Untuk melakukan proses tersebut, bisa dilakukan dengan potong kode berikut.
```
    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE); 
    if(child_id == 0){
        char *argv[] = {"touch","home/fany/modul2/air/list.txt", NULL};
        execv("/bin/touch", argv);
    }      
    while((wait(&status)) > 0);

    FILE *list;
    list = fopen("home/fany/modul2/air/list.txt","w+");

    dp = opendir(banyu);

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strstr(ep->d_name,"air") != NULL){
                struct stat fs;
                int r;
                char filee[200] = "home/fany/modul2/air/";
                strcat(filee,ep->d_name);

                r = stat(filee,&fs);
                if(r == -1)
                {
                    fprintf(stderr,"File error\n");
                    exit(1);
                }
                struct passwd *pw = getpwuid(fs.st_uid);
                char namafile[150]="";
                strcat(namafile,pw->pw_name);
                strcat(namafile,"_");
                if( fs.st_mode & S_IRUSR )  strcat(namafile,"r");
                if( fs.st_mode & S_IWUSR )  strcat(namafile,"w");
                if( fs.st_mode & S_IXUSR )  strcat(namafile,"x");
                strcat(namafile,"_");
                strcat(namafile,ep->d_name);
                fprintf(list,"%s\n",namafile);
            }            
        }
        (void) closedir(dp);
    } else perror ("Couldn't open the directory");
```
