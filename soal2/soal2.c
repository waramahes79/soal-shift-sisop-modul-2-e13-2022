#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>

void Createdir(){
    char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
    execv("/bin/mkdir", argv);
}

void unzip_drakor(){
    char *argv[] = {"unzip", "-n", "drakor.zip", "*.png", NULL};
    execv("/bin/unzip", argv);
}

void mkdir_custom(char* dir){
    char *argv[] = {"mkdir", "-p", dir, NULL};
    execv("/bin/mkdir", argv);
}

void split(char* filename){
    char* token1;
    int count = 0;

    token1 = strtok(filename, "_;.");
   
    while( token1 != NULL ) {
        count++;
        if(count == 3 || count == 6){
            pid_t child_id;
            int status;
            child_id = fork();
            if (child_id < 0) exit(EXIT_FAILURE);

            if (child_id == 0) {
                mkdir_custom(token1);
            } else while ((wait(&status)) > 0);
        }
        
        token1 = strtok(NULL, "_;.");
    }
}

void dircheck(){
    DIR *dp;
    struct dirent *ep;

    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) split(ep->d_name);

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void png_move(){
    DIR *dp;
    struct dirent *ep;
    FILE *fptr1, *fptr2;

    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) {
        char* token1, pngname[3][50], dirname[300], temp, strtemp[280];
        int count = 0;
        FILE *fptr;
        strcpy(strtemp, ep->d_name);

        token1 = strtok(strtemp, "_;.");
    
        while( token1 != NULL ) {
            count++;
            if ((count == 1 || count == 4) && strcmp(token1, "png") != 0){
                strcpy(pngname[0], token1);
            }
            else if (count == 2 || count == 5){
                strcpy(pngname[1], token1);
            }
            else if (count == 3 || count == 6){
                strcpy(pngname[2], token1);
                sprintf(dirname, "%s/%s_%s_%s.png", pngname[2], pngname[1], pngname[0], pngname[2]);
                fptr1 = fopen(ep->d_name, "rb");
                fptr2 = fopen(dirname, "wb");

                while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

                fclose(fptr1);
                fclose(fptr2);
            }
            
            token1 = strtok(NULL, "_;.");
            }
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void genre_check(char *dir){
    DIR *dp;
    struct dirent *ep;
    int index = 0, count, status;
    char poster_name[10][280], temp[280], *token, name[280], year[280], dirname[100], temp2[290], temp3[290];
    FILE *fptr;
    pid_t child_id;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL){
          strcpy(poster_name[index++], ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    for(int i=0; i<index-1; i++)for (int j=0; j<index-i-1; j++){
        if(strcmp(poster_name[j], poster_name[j+1]) > 0){
            strcpy(temp, poster_name[j]);
            strcpy(poster_name[j], poster_name[j+1]);
            strcpy(poster_name[j+1], temp);
        }
    }
    sprintf(dirname, "%s/data.txt", dir);
    fptr = fopen(dirname, "w");
    fprintf(fptr, "kategori : %s\n", dir);
    for(int i=0; i<index; i++) {
        strcpy(temp, poster_name[i]);
        count = 0;
        token = strtok(temp, "_");
        while(token != NULL){
            count++;
            if(count == 1) strcpy(year, token);
            else if (count == 2) strcpy(name, token);
            token = strtok(NULL, "_");
        }
        fprintf(fptr, "\nnama : %s", name);
        fprintf(fptr, "\nrilis : %s\n", year);
        sprintf(temp3, "%s/%s.png", dir, name);
        sprintf(temp2, "%s/%s", dir, poster_name[i]);
        child_id = fork();
        if (child_id < 0) exit(EXIT_FAILURE);

        if (child_id == 0) {
                char *argv[] = {"mv", temp2, temp3, NULL};
                execv("/bin/mv", argv);

        } else while ((wait(&status)) > 0);
    }
    fclose(fptr);
}

void clear(){
    DIR *dp;
    struct dirent *ep;
    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL){
          remove(ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

int main(){
    pid_t child_id;
    int status;
    char dirname[100], user[20];
    strcpy(user, getenv("USER"));
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir("home")) < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir(user)) < 0) {
        exit(EXIT_FAILURE);
    }

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        Createdir();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        if ((chdir("shift2/drakor")) < 0) exit(EXIT_FAILURE);
        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }

        if (child_id == 0) {
            // this is child
            zip_download();
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
            }

            if (child_id == 0) {
                // this is child
                unzip_drakor();
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                child_id = fork();
                if (child_id < 0) {
                    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                }

                if (child_id == 0) {
                    // this is child

                } else {
                    // this is parent
                    while ((wait(&status)) > 0);
                    dircheck();

                    png_move();

                    genre_check("action");
                    genre_check("comedy");
                    genre_check("fantasy");
                    genre_check("horror");
                    genre_check("romance");
                    genre_check("school");
                    genre_check("thriller");

                    clear();
                }
            }
        }
    }

    return 0;
}