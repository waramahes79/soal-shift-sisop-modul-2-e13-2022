#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <json-c/json.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

pid_t child_id;
int status;

void download(char* link)
{
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) 
    {
        printf("child PID = %d\n", getpid());
        char *argv[] = {"/usr/bin/wget", "-nd", link, NULL};
        execv("/usr/bin/wget", argv);
    }           
    while ((wait(&status)) > 0);
}

void ekstrak(char* file)
{
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) 
    {
        char *argv[] = {"/bin/unzip", file, NULL};
        execv("/bin/unzip", argv);
    } 
    while ((wait(&status)) > 0);
}

void buatFolder(char* nama)
{
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) 
    {
        char *argv[] = {"/usr/bin/mkdir", nama, NULL};
        execv("/usr/bin/mkdir", argv);
    } 
    while ((wait(&status)) > 0);
}

void pindahin(char* objek, char* tujuan)
{
    // memindahkan foder objek ke folder tujuan
    child_id = fork();
  
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) 
    {
        char *argv[] = {"/usr/bin/mv", objek, tujuan, NULL};
        execv("/usr/bin/mv", argv);
    } 
    while ((wait(&status)) > 0);
}

int main(int argc)
{

    /* SOAL 1.a */ 
    char link_weapons[] = "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char link_characters[] = "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
    char file_weapons[] = "uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char file_characters[] = "uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";

    download(link_weapons);
    ekstrak(file_weapons);
    download(link_characters);
    ekstrak(file_characters);

    buatFolder("gacha_gacha");


    /* SOAL 1.b, c, d */ 
    int i = 1, j;
    int i_file;
    char str_i[50]; //i tapi versi string
    int n = 270;
    int primogems = 79000;
    char str_primogems[50]; //primogems versi string
    FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

    char nama_folder[100]; 
    char sjumlah[100];
    char nama_file[100];
    char arr_file[100][100];
    char path[100];
    DIR *dp;
    struct dirent *ep;
    char arr_weapons[1000][100];
    char arr_char[1000][100];

    char temp_gacha[11][200];

    // menyalin nama item (untuk keperluan selanjutnya)

    // mengambil gacha item weapons jika genap
    int idw = 0;
    strcpy(path, "./weapons");

    dp = opendir(path);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            strcpy(arr_weapons[idw++], ep->d_name);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    // mengambil gacha item characters jika genap
    int idc = 0;
    strcpy(path, "./characters");

    dp = opendir(path);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            strcpy(arr_char[idc++], ep->d_name);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    
    while(primogems >= 160)
    {   
        // setiap gacha menghabiskan 160 primogems
        primogems -= 160;
        
        int random = rand() % 47;
        char path_char[50] = "characters/";
        char path_weapons[50] = "weapons/";
        strcat(path_char, arr_char[random]);
        strcat(path_weapons, arr_weapons[random]);
        
        sprintf(str_i, "%d", i);

        int i_temp = i % 10; //index untuk array yg menampung data sementara
        strcpy(temp_gacha[i_temp], str_i);

        if(i%2 == 0)
        {
            // jika genap, gacha diambil dari characters
            fp = fopen(path_char, "r");
            fread(buffer, 3000, 1, fp);
            fclose(fp);
            
            parsed_json = json_tokener_parse(buffer);
            json_object_object_get_ex(parsed_json, "name", &name);
            json_object_object_get_ex(parsed_json, "rarity", &rarity);

            strcpy(temp_gacha[i_temp], "_characters_");
        }
        else
        {
            // jika ganjil, gacha diambil dari weapons
            fp = fopen(path_weapons, "r");
            fread(buffer, 3000, 1, fp);
            fclose(fp);
            
            parsed_json = json_tokener_parse(buffer);
            json_object_object_get_ex(parsed_json, "name", &name);
            json_object_object_get_ex(parsed_json, "rarity", &rarity);
            strcpy(temp_gacha[i_temp], "_weapons_");
        }
        
        strcat(temp_gacha[i_temp], "");
        strcat(temp_gacha[i_temp], json_object_get_string(rarity));
        strcat(temp_gacha[i_temp], "_");
        strcat(temp_gacha[i_temp], json_object_get_string(name));
        strcat(temp_gacha[i_temp], "_");

        sprintf(str_primogems, "%d", primogems);
        strcat(temp_gacha[i_temp], str_primogems);

        if(i%10 == 0)
        {

            time_t t = time(NULL);
            struct tm tm = *localtime(&t);
            sprintf(nama_file, "./gacha_gacha/%02d:%02d%02d_gacha_%d.txt", tm.tm_hour, tm.tm_min, tm.tm_sec, i);

            strcpy(arr_file[i_file], nama_file);

            FILE *fp1;
            fp1 = fopen(nama_file, "w+");
            for(j = 1; j<10; j++)
            {
                fputs(temp_gacha[j], fp1);
                fputs("\n", fp1);
            }
            fputs(temp_gacha[0], fp1);
            fputs("\n", fp1);
            fclose(fp1);
        }

        if(i%90 == 0)
        {
            // membuat folder baru dengan format nama total_gacha_{jumlah-gacha}
            sprintf(sjumlah, "%d", i);
            strcpy(nama_folder, "total_gacha_");
            strcat(nama_folder, sjumlah);

            buatFolder(nama_folder);
            
            for(j=0; j<10; j++)
            {
                child_id = fork();

                if (child_id < 0) {
                    exit(EXIT_FAILURE);
                }
                if (child_id == 0) 
                {
                    char *argv[] = {"/usr/bin/mv", arr_file[j], nama_folder, NULL};
                    execv("/usr/bin/mv", argv);
                } 
                while ((wait(&status)) > 0);
            }
            
        }

        i++;
    }

}
